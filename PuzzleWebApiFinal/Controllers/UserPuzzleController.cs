﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PuzzleWebApiFinal.Models;

namespace PuzzleWebApiFinal.Controllers
{
    public class UserPuzzleController : ApiController
    {
        private PuzzleGameDBEntities3 db = new PuzzleGameDBEntities3();

        // GET api/UserPuzzle
        public IQueryable<usersPuzzles> GetusersPuzzles()
        {
            return db.usersPuzzles;
        }

        // GET api/UserPuzzle/5
        [ResponseType(typeof(usersPuzzles))]
        public IHttpActionResult GetusersPuzzles(int id)
        {
            usersPuzzles userspuzzles = db.usersPuzzles.Find(id);
            if (userspuzzles == null)
            {
                return NotFound();
            }

            return Ok(userspuzzles);
        }

        // PUT api/UserPuzzle/5
        public IHttpActionResult PutusersPuzzles(int id, usersPuzzles userspuzzles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userspuzzles.userId)
            {
                return BadRequest();
            }

            db.Entry(userspuzzles).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!usersPuzzlesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/UserPuzzle
        [ResponseType(typeof(usersPuzzles))]
        public IHttpActionResult PostusersPuzzles(usersPuzzles userspuzzles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.usersPuzzles.Add(userspuzzles);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (usersPuzzlesExists(userspuzzles.userId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = userspuzzles.userId }, userspuzzles);
        }

        // DELETE api/UserPuzzle/5
        [ResponseType(typeof(usersPuzzles))]
        public IHttpActionResult DeleteusersPuzzles(int id)
        {
            usersPuzzles userspuzzles = db.usersPuzzles.Find(id);
            if (userspuzzles == null)
            {
                return NotFound();
            }

            db.usersPuzzles.Remove(userspuzzles);
            db.SaveChanges();

            return Ok(userspuzzles);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool usersPuzzlesExists(int id)
        {
            return db.usersPuzzles.Count(e => e.userId == id) > 0;
        }
    }
}