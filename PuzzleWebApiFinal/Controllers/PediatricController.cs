﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PuzzleWebApiFinal.Models;

namespace PuzzleWebApiFinal.Controllers
{
    public class PediatricController : ApiController
    {
        private PuzzleGameDBEntities4 db = new PuzzleGameDBEntities4();

        // GET api/Pediatric
        public IQueryable<pediatrics> Getpediatrics()
        {
            return db.pediatrics;
        }

        // GET api/Pediatric/5
        [ResponseType(typeof(pediatrics))]
        public IHttpActionResult Getpediatrics(int id)
        {
            pediatrics pediatrics = db.pediatrics.Find(id);
            if (pediatrics == null)
            {
                return NotFound();
            }

            return Ok(pediatrics);
        }

        // PUT api/Pediatric/5
        public IHttpActionResult Putpediatrics(int id, pediatrics pediatrics)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pediatrics.id)
            {
                return BadRequest();
            }

            db.Entry(pediatrics).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!pediatricsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Pediatric
        [ResponseType(typeof(pediatrics))]
        public IHttpActionResult Postpediatrics(pediatrics pediatrics)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.pediatrics.Add(pediatrics);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pediatrics.id }, pediatrics);
        }

        // DELETE api/Pediatric/5
        [ResponseType(typeof(pediatrics))]
        public IHttpActionResult Deletepediatrics(int id)
        {
            pediatrics pediatrics = db.pediatrics.Find(id);
            if (pediatrics == null)
            {
                return NotFound();
            }

            db.pediatrics.Remove(pediatrics);
            db.SaveChanges();

            return Ok(pediatrics);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool pediatricsExists(int id)
        {
            return db.pediatrics.Count(e => e.id == id) > 0;
        }
    }
}