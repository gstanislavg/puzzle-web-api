﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PuzzleWebApiFinal.Models;

namespace PuzzleWebApiFinal.Controllers
{
    public class PuzzleController : ApiController
    {
        private PuzzleGameDBEntities1 db = new PuzzleGameDBEntities1();

        // GET api/Puzzle
        public IQueryable<puzzles> Getpuzzles()
        {
            return db.puzzles;
        }

        // GET api/Puzzle/5
        [ResponseType(typeof(puzzles))]
        public IHttpActionResult Getpuzzles(int id)
        {
            puzzles puzzles = db.puzzles.Find(id);
            if (puzzles == null)
            {
                return NotFound();
            }

            return Ok(puzzles);
        }

        public IHttpActionResult GetPuzzlesForPediatrician(int id)
        {
            var puzzles = db.puzzles.Where(a => a.pediatricId == id).ToList();

            if (puzzles == null || puzzles.Count == 0)
            {
                return NotFound();
            }

            return Ok(puzzles);
        }

        [HttpPost]
        [Route("PuzzleAndPieces")]
        public IHttpActionResult PostPuzzleAndPieces(InsertPuzzlePiecesModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var guidFolderName = Guid.NewGuid();
            var path = string.Format("D:\\\\puzzles\\{0}", guidFolderName);
            System.IO.Directory.CreateDirectory(path);
            var mainPuzzleImagePath = string.Format(path + "\\{0}.png", model.puzzleModel.puzzleName);

            var puzzleEntity = new puzzle
            {
                difficulty = model.puzzleModel.difficulty,
                pediatricId = model.puzzleModel.pediatricId,
                puzzleImage = mainPuzzleImagePath,
                puzzleName = model.puzzleModel.puzzleName
            };

            var byteArray = Convert.FromBase64String(model.puzzleModel.puzzleImage);
            Image receivedImage;
            using (var ms = new MemoryStream(byteArray))
            {
                receivedImage = Image.FromStream(ms);
            }

            receivedImage.Save(mainPuzzleImagePath);

            db.puzzles.Add(puzzleEntity);
            db.SaveChanges();

            foreach (var piece in model.piecesModel)
            {
                var piecePuzzleImagePath = string.Format(path + "\\{0}.png", piece.puzzlePieceName);
                var puzzlePieceEntity = new puzzlePiece
                {
                    pieceImage = piecePuzzleImagePath,
                    puzzleId = puzzleEntity.id,
                    puzzlePieceName = piece.puzzlePieceName
                };

                db3.puzzlePieces.Add(puzzlePieceEntity);

                var byteArray1 = Convert.FromBase64String(piece.pieceImage);
                Image receivedImage1;
                using (var ms = new MemoryStream(byteArray1))
                {
                    receivedImage1 = Image.FromStream(ms);
                }

                receivedImage.Save(piecePuzzleImagePath);
            }

            db3.SaveChanges();

            return Ok();
        }



        // PUT api/Puzzle/5
        public IHttpActionResult Putpuzzles(int id, puzzles puzzles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != puzzles.id)
            {
                return BadRequest();
            }

            db.Entry(puzzles).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!puzzlesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Puzzle
        [ResponseType(typeof(puzzles))]
        public IHttpActionResult Postpuzzles(puzzles puzzles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.puzzles.Add(puzzles);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = puzzles.id }, puzzles);
        }

        // DELETE api/Puzzle/5
        [ResponseType(typeof(puzzles))]
        public IHttpActionResult Deletepuzzles(int id)
        {
            puzzles puzzles = db.puzzles.Find(id);
            if (puzzles == null)
            {
                return NotFound();
            }

            db.puzzles.Remove(puzzles);
            db.SaveChanges();

            return Ok(puzzles);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool puzzlesExists(int id)
        {
            return db.puzzles.Count(e => e.id == id) > 0;
        }
    }
}