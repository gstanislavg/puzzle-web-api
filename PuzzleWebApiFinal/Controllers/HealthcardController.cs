﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PuzzleWebApiFinal.Models;

namespace PuzzleWebApiFinal.Controllers
{
    public class HealthcardController : ApiController
    {
        private PuzzleGameDBEntities db = new PuzzleGameDBEntities();

        // GET api/Healthcard
        public IQueryable<healthCard> GethealthCard()
        {
            return db.healthCard;
        }

        // GET api/Healthcard/5
        [ResponseType(typeof(healthCard))]
        public IHttpActionResult GethealthCard(int id)
        {
            healthCard healthcard = db.healthCard.Find(id);
            if (healthcard == null)
            {
                return NotFound();
            }

            return Ok(healthcard);
        }

        // PUT api/Healthcard/5
        public IHttpActionResult PuthealthCard(int id, healthCard healthcard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != healthcard.id)
            {
                return BadRequest();
            }

            db.Entry(healthcard).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!healthCardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Healthcard
        [ResponseType(typeof(healthCard))]
        public IHttpActionResult PosthealthCard(healthCard healthcard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.healthCard.Add(healthcard);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = healthcard.id }, healthcard);
        }

        // DELETE api/Healthcard/5
        [ResponseType(typeof(healthCard))]
        public IHttpActionResult DeletehealthCard(int id)
        {
            healthCard healthcard = db.healthCard.Find(id);
            if (healthcard == null)
            {
                return NotFound();
            }

            db.healthCard.Remove(healthcard);
            db.SaveChanges();

            return Ok(healthcard);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool healthCardExists(int id)
        {
            return db.healthCard.Count(e => e.id == id) > 0;
        }
    }
}