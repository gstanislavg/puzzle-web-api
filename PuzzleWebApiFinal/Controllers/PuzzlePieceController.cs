﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PuzzleWebApiFinal.Models;

namespace PuzzleWebApiFinal.Controllers
{
    public class PuzzlePieceController : ApiController
    {
        private PuzzleGameDBEntities2 db = new PuzzleGameDBEntities2();

        // GET api/PuzzlePiece
        public IQueryable<puzzlePieces> GetpuzzlePieces()
        {
            return db.puzzlePieces;
        }

        // GET api/PuzzlePiece/5
        [ResponseType(typeof(puzzlePieces))]
        public IHttpActionResult GetpuzzlePieces(int id)
        {
            puzzlePieces puzzlepieces = db.puzzlePieces.Find(id);
            if (puzzlepieces == null)
            {
                return NotFound();
            }

            return Ok(puzzlepieces);
        }

        // PUT api/PuzzlePiece/5
        public IHttpActionResult PutpuzzlePieces(int id, puzzlePieces puzzlepieces)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != puzzlepieces.id)
            {
                return BadRequest();
            }

            db.Entry(puzzlepieces).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!puzzlePiecesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/PuzzlePiece
        [ResponseType(typeof(puzzlePieces))]
        public IHttpActionResult PostpuzzlePieces(puzzlePieces puzzlepieces)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.puzzlePieces.Add(puzzlepieces);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = puzzlepieces.id }, puzzlepieces);
        }

        // DELETE api/PuzzlePiece/5
        [ResponseType(typeof(puzzlePieces))]
        public IHttpActionResult DeletepuzzlePieces(int id)
        {
            puzzlePieces puzzlepieces = db.puzzlePieces.Find(id);
            if (puzzlepieces == null)
            {
                return NotFound();
            }

            db.puzzlePieces.Remove(puzzlepieces);
            db.SaveChanges();

            return Ok(puzzlepieces);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool puzzlePiecesExists(int id)
        {
            return db.puzzlePieces.Count(e => e.id == id) > 0;
        }
    }
}