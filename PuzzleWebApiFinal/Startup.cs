﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PuzzleWebApiFinal.Startup))]
namespace PuzzleWebApiFinal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
