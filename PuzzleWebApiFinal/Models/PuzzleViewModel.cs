﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuzzleWebApiFinal.Models
{
    public class PuzzleViewModel
    {
        public string puzzleName { get; set; }

        public string difficulty { get; set; }

        public Nullable<int> pediatricId { get; set; }

        public string puzzleImage { get; set; }
    }
}