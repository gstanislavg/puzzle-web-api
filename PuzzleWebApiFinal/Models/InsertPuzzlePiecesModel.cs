﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuzzleWebApiFinal.Models
{
    public class InsertPuzzlePiecesModel
    {
        public InsertPuzzlePiecesModel()
        {
            piecesModel = new List<PuzzlePieceViewModel>();
        }

        public PuzzleViewModel puzzleModel { get; set; }

        public List<PuzzlePieceViewModel> piecesModel { get; set; }
    }
}