﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuzzleWebApiFinal.Models
{
    public class PuzzlePieceViewModel
    {
        public string puzzlePieceName { get; set; }

        public string pieceImage { get; set; }
    }
}