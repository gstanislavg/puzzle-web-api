﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PuzzleAPI.Models;

namespace PuzzleAPI.Controllers
{
    public class UserController : ApiController
    {
        private PuzzleGameDBEntities1 db = new PuzzleGameDBEntities1();

        // GET api/User
        public IQueryable<users> Getusers()
        {
            return db.users;
        }

        // GET api/User/5
        [ResponseType(typeof(users))]
        public IHttpActionResult Getusers(int id)
        {
            users users = db.users.Find(id);
            if (users == null)
            {
                return NotFound();
            }

            return Ok(users);
        }

        // PUT api/User/5
        public IHttpActionResult Putusers(int id, users users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != users.id)
            {
                return BadRequest();
            }

            db.Entry(users).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!usersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [Route("usersForPediatric/{pediatricId}")]
        [HttpGet]
        public IHttpActionResult GetUsersForPediatric(int pediatricId)
        {
            var users = db.users.Where(p => p.pediatricId == pediatricId).ToList();
            var usersList = new List<users>();

            if (users.Count == 0)
            {
                return BadRequest();
            }

            foreach (var user in users)
            {
                usersList.Add(user);
            }

            return Ok(usersList);
        }

        // POST api/User
        [ResponseType(typeof(users))]
        public IHttpActionResult Postusers(users users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.users.Add(users);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = users.id }, users);
        }

        // DELETE api/User/5
        [ResponseType(typeof(users))]
        public IHttpActionResult Deleteusers(int id)
        {
            users users = db.users.Find(id);
            if (users == null)
            {
                return NotFound();
            }

            db.users.Remove(users);
            db.SaveChanges();

            return Ok(users);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool usersExists(int id)
        {
            return db.users.Count(e => e.id == id) > 0;
        }


        [Route("deleteUser/{userId}")]
        [HttpDelete]
        public IHttpActionResult DeleteUser(int userId)
        {
            var puzzle = db.users.Find(userId);

            if (puzzle == null)
            {
                return NotFound();
            }

            db.users.Remove(puzzle);
            db.SaveChanges();

            return Ok();
        }
        

        [HttpPost]
        [Route("userExists")]
        public IHttpActionResult UserExists(users user)
        {
            var myUser = db.users.ToList().Where(u => u.uName.ToString
                ().Equals(user.uName.ToString()) && u.uPassword.ToString().Equals(user.uPassword.ToString())).FirstOrDefault(); //FirstOrDefault(u => u.uName == user.uName && u.uPassword == user.uPassword);

            if (myUser == null)
            {
                return NotFound(); 
            }

            return Ok(myUser);
        }


    }
}