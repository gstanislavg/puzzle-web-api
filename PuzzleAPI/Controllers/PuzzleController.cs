﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web;
using System.Drawing;
using System.IO;
using PuzzleAPI.Models;

namespace puzzlesGameWebApi.Controllers
{
    public class puzzlesController : ApiController
    {
        private PuzzleGameDBEntities db = new PuzzleGameDBEntities();
        private PuzzleGameDBEntities5 db3 = new PuzzleGameDBEntities5();
        private PuzzleGameDBEntities2 db2 = new PuzzleGameDBEntities2();

        // GET api/puzzles
        public IQueryable<puzzles> Getpuzzless()
        {
            return db.puzzles;
        }

        // GET api/puzzles/5
        [ResponseType(typeof(puzzles))]
        public IHttpActionResult Getpuzzles(int id)
        {
            puzzles puzzles = db.puzzles.Find(id);
            if (puzzles == null)
            {
                return NotFound();
            }

            return Ok(puzzles);
        }

        public IHttpActionResult GetpuzzlessForPediatrician(int id)
        {
            var puzzless = db.puzzles.Where(a => a.pediatricId == id).ToList();

            if (puzzless == null || puzzless.Count == 0)
            {
                return NotFound();
            }

            return Ok(puzzless);
        }

        [HttpPost]
        [Route("puzzleAndPieces")]
        public IHttpActionResult PostpuzzlesAndPieces(InsertpuzzlesPiecesModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var guidFolderName = Guid.NewGuid();
            var path = string.Format("D:\\\\puzzles\\{0}", guidFolderName);
            System.IO.Directory.CreateDirectory(path);
            var mainpuzzlesImagePath = string.Format(path + "\\{0}.png", model.puzzleModel.puzzleName);

            var puzzlesEntity = new puzzles
            {
                difficulty = model.puzzleModel.difficulty,
                pediatricId = model.puzzleModel.pediatricId,
                puzzleImage = mainpuzzlesImagePath,
                puzzleName = model.puzzleModel.puzzleName
            };

            var byteArray = Convert.FromBase64String(model.puzzleModel.puzzleImage);
            Image receivedImage;
            using (var ms = new MemoryStream(byteArray))
            {
                receivedImage = Image.FromStream(ms);
            }

            receivedImage.Save(mainpuzzlesImagePath);

            db.puzzles.Add(puzzlesEntity);
            db.SaveChanges();

            foreach (var piece in model.piecesModel)
            {
                var piecepuzzlesImagePath = string.Format(path + "\\{0}.png", piece.puzzlePieceName);
                var puzzlesPieceEntity = new puzzlePieces
                {
                    pieceImage = piecepuzzlesImagePath,
                    puzzleId = puzzlesEntity.id,
                    puzzlePieceName = piece.puzzlePieceName
                };

                db3.puzzlePieces.Add(puzzlesPieceEntity);

                var byteArray1 = Convert.FromBase64String(piece.pieceImage);
                Image receivedImage1;
                using (var ms = new MemoryStream(byteArray1))
                {
                    receivedImage1 = Image.FromStream(ms);
                }

                receivedImage1.Save(piecepuzzlesImagePath);
            }

            db3.SaveChanges();

            return Ok();
        }

        [Route("puzzlesForUser/{userId}")]
        [HttpGet]

        public IHttpActionResult GetPuzzlesForUser(int userId)
        {
            var puzzleIds = db2.usersPuzzles.Where(p => p.userId == userId).ToList();

            if (puzzleIds.Count == 0)
            {
                return NotFound();
            }

            var puzzles = new List<puzzles>();

            foreach (var puzzleId in puzzleIds) 
            {
                var puzzle = db.puzzles.Find(puzzleId.puzzleId);

                puzzles.Add(puzzle);
            }

            var puzzleImages = new List<PuzzleImage>();

            foreach (var puzzle in puzzles)
            {
                if (!string.IsNullOrEmpty(puzzle.puzzleImage))
                {
                    using (var image = Image.FromFile(puzzle.puzzleImage, false))
                    {
                        puzzleImages.Add(new PuzzleImage
                        {
                            puzzleImage = Convert.ToBase64String(ImageToByteArray(image)),
                            id = puzzle.id,
                            puzzleName = puzzle.puzzleName,
                            difficulty = puzzle.difficulty
                        });
                    }
                }
            }

            if (puzzleImages.Count < 1)
            {
                return NotFound();
            }

            return Ok(puzzleImages);
        }

        [Route("deletePuzzle/{puzzleId}")]
        [HttpDelete]
        public IHttpActionResult DeletePuzzle(int puzzleId)
        {
            var puzzle = db.puzzles.Find(puzzleId);

            if (puzzle == null) 
            {
                return NotFound();
            }

            db.puzzles.Remove(puzzle);
            db.SaveChanges();

            return Ok();
        }

        [Route("puzzlesForPediatric/{pediatricId}")]
        [HttpGet]
        public IHttpActionResult GetPuzzleForPediatric(int pediatricId)
        {
            var puzzles = db.puzzles.Where(p => p.pediatricId == pediatricId).ToList();
            var puzzleImages = new List<PuzzleImage>();

            if (puzzles.Count == 0)
            {
                return BadRequest();
            }

            foreach (var puzzle in puzzles)
            {
                if (!string.IsNullOrEmpty(puzzle.puzzleImage))
                {
                    using (var image = Image.FromFile(puzzle.puzzleImage, false))
                    {
                        puzzleImages.Add(new PuzzleImage
                        {
                            puzzleImage = Convert.ToBase64String(ImageToByteArray(image)),
                            id = puzzle.id,
                            puzzleName = puzzle.puzzleName,
                            difficulty = puzzle.difficulty
                        });
                    }
                }
            }

            if (puzzleImages.Count < 1)
            {
                return BadRequest();
            }

            return Ok(puzzleImages);
        }

        // PUT api/puzzles/5
        public IHttpActionResult Putpuzzles(int id, puzzles puzzles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != puzzles.id)
            {
                return BadRequest();
            }

            db.Entry(puzzles).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!puzzlesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/puzzles
        [ResponseType(typeof(puzzles))]
        public IHttpActionResult Postpuzzles(puzzles puzzles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.puzzles.Add(puzzles);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = puzzles.id }, puzzles);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool puzzlesExists(int id)
        {
            return db.puzzles.Count(e => e.id == id) > 0;
        }

        private byte[] ImageToByteArray(Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                var data = new byte[ms.Length];
                ms.Write(data, 0, data.Length);
                return ms.ToArray();
            }
        }
    }
}