﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Threading.Tasks;
using PuzzleAPI.Models;
using System.Drawing;
using System.IO;

namespace PuzzleGameWebApi.Controllers
{
    public class PuzzlePiecesController : ApiController
    {
        private PuzzleGameDBEntities5 db = new PuzzleGameDBEntities5();

        // GET api/PuzzlePieces
        public IQueryable<puzzlePieces> GetpuzzlePieces()
        {
            return db.puzzlePieces;
        }

        // GET api/PuzzlePieces/1
        public IHttpActionResult GetPuzzlePieces(int id)
        {
            var pieces = db.puzzlePieces.Where(a => a.puzzleId == id).ToList();

            if (pieces == null || pieces.Count == 0)
            {
                return NotFound();
            }

            return Ok(pieces);
        }

        [Route("puzzlePiecesImages/{puzzleId}")]
        [HttpGet]
        public IHttpActionResult GetPuzzleForUser(int puzzleId)
        {
            var puzzlePieces = db.puzzlePieces.Where(p => p.puzzleId == puzzleId).ToList();
            var puzzles = new List<PuzzlePieceImage>();

            if (puzzlePieces == null)
            {
                return NotFound();
            }

            foreach (var piece in puzzlePieces)
            {
                using (var image = Image.FromFile(piece.pieceImage, false))
                {
                    puzzles.Add(new PuzzlePieceImage
                    {
                        pieceImage = Convert.ToBase64String(ImageToByteArray(image)),
                        id = piece.id,
                        puzzlePieceName = piece.puzzlePieceName
                    });
                }
            }

            if (puzzles.Count < 1)
            {
                return NotFound();
            }

            return Ok(puzzles);
        }

        // GET api/PuzzlePieces/5
        [ResponseType(typeof(puzzlePieces))]
        public IHttpActionResult GetpuzzlePiece(int id)
        {
            puzzlePieces puzzlepiece = db.puzzlePieces.Find(id);
            if (puzzlepiece == null)
            {
                return NotFound();
            }

            return Ok(puzzlepiece);
        }

        // PUT api/PuzzlePieces/5
        public IHttpActionResult PutpuzzlePiece(int id, puzzlePieces puzzlepiece)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != puzzlepiece.id)
            {
                return BadRequest();
            }

            db.Entry(puzzlepiece).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!puzzlePieceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/PuzzlePieces
        [ResponseType(typeof(puzzlePieces))]
        public IHttpActionResult PostpuzzlePiece(puzzlePieces puzzlepiece)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.puzzlePieces.Add(puzzlepiece);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = puzzlepiece.id }, puzzlepiece);
        }

        // DELETE api/PuzzlePieces/5
        [ResponseType(typeof(puzzlePieces))]
        public IHttpActionResult DeletepuzzlePiece(int id)
        {
            puzzlePieces puzzlepiece = db.puzzlePieces.Find(id);
            if (puzzlepiece == null)
            {
                return NotFound();
            }

            db.puzzlePieces.Remove(puzzlepiece);
            db.SaveChanges();

            return Ok(puzzlepiece);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool puzzlePieceExists(int id)
        {
            return db.puzzlePieces.Count(e => e.id == id) > 0;
        }

        private byte[] ImageToByteArray(Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                var data = new byte[ms.Length];
                ms.Write(data, 0, data.Length);
                return ms.ToArray();
            }
        }
    }
}