﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PuzzleAPI.Models;

namespace PuzzleAPI.Controllers
{
    public class PediatricsController : ApiController
    {
        private PuzzleGameDBEntities3 db = new PuzzleGameDBEntities3();

        // GET api/Pediatrics
        public IQueryable<pediatrics> Getpediatrics()
        {
            return db.pediatrics;
        }

        // GET api/Pediatrics/5
        [ResponseType(typeof(pediatrics))]
        public IHttpActionResult Getpediatrics(int id)
        {
            pediatrics pediatrics = db.pediatrics.Find(id);
            if (pediatrics == null)
            {
                return NotFound();
            }

            return Ok(pediatrics);
        }

        // PUT api/Pediatrics/5
        public IHttpActionResult Putpediatrics(int id, pediatrics pediatrics)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pediatrics.id)
            {
                return BadRequest();
            }

            db.Entry(pediatrics).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!pediatricsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Pediatrics
        [ResponseType(typeof(pediatrics))]
        public IHttpActionResult Postpediatrics(pediatrics pediatrics)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.pediatrics.Add(pediatrics);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pediatrics.id }, pediatrics);
        }

        // DELETE api/Pediatrics/5
        [ResponseType(typeof(pediatrics))]
        public IHttpActionResult Deletepediatrics(int id)
        {
            pediatrics pediatrics = db.pediatrics.Find(id);
            if (pediatrics == null)
            {
                return NotFound();
            }

            db.pediatrics.Remove(pediatrics);
            db.SaveChanges();

            return Ok(pediatrics);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool pediatricsExists(int id)
        {
            return db.pediatrics.Count(e => e.id == id) > 0;
        }

        [HttpPost]
        [Route("pediatricExists")]
        public IHttpActionResult PediatricExists(pediatrics pediatric)
        {
            var myUser = db.pediatrics.FirstOrDefault(u => u.pName == pediatric.pName && u.pPassword == pediatric.pPassword);

            if (myUser == null)
            {
                return BadRequest();
            }

            return Ok(myUser);
        }
    }
}