﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuzzleAPI.Models
{
    public class PuzzleImage
    {
        public int id { get; set; }
        public string puzzleName { get; set; }
        public string difficulty { get; set; }
        public string puzzleImage { get; set; }
    }
}