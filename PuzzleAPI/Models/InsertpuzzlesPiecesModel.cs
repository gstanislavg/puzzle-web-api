﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuzzleAPI.Models
{
    public class InsertpuzzlesPiecesModel
    {
        public InsertpuzzlesPiecesModel()
        {
            piecesModel = new List<PuzzlePieceViewModel>();
        }

        public PuzzleViewModel puzzleModel { get; set; }

        public List<PuzzlePieceViewModel> piecesModel { get; set; }
    }
}