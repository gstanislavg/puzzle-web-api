﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuzzleAPI.Models
{
    public class PuzzlePieceImage
    {
        public int id { get; set; }
        public string puzzlePieceName { get; set; }
        public string pieceImage { get; set; }
    }
}